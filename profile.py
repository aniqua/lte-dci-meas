#!/usr/bin/python

"""
This profile is for collecting LTE DCI data.

"""

# Library imports
import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab.pnext as pn
import geni.rspec.emulab.spectrum as spectrum
import geni.rspec.igext as ig
import geni.rspec.emulab.route as route



# Global Variables
images = {
    "U18-GR" : 'urn:publicid:IDN+emulab.net+image+PowderTeam:U18-GR-PBUF'
}
images_list = list(images.keys())



# List of monitor radios
monitor_radios = [
    ("cellsdr1-hospital",
     "Hospital"),
    ("cbrssdr1-honors",
     "Honors"),
]


# A list of endpoint sites
fe_sites = [
    ('urn:publicid:IDN+bookstore.powderwireless.net+authority+cm',
     "Bookstore"),
    ('urn:publicid:IDN+cpg.powderwireless.net+authority+cm',
     "Garage"),
    ('urn:publicid:IDN+ebc.powderwireless.net+authority+cm',
     "EBC"),
    ('urn:publicid:IDN+guesthouse.powderwireless.net+authority+cm',
     "GuestHouse"),
    ('urn:publicid:IDN+humanities.powderwireless.net+authority+cm',
     "Humanities"),
    ('urn:publicid:IDN+law73.powderwireless.net+authority+cm',
     "Law73"),
    ('urn:publicid:IDN+madsen.powderwireless.net+authority+cm',
     "Madsen"),
    ('urn:publicid:IDN+moran.powderwireless.net+authority+cm',
     "Moran"),
    ('urn:publicid:IDN+sagepoint.powderwireless.net+authority+cm',
     "SagePoint"),
    ('urn:publicid:IDN+web.powderwireless.net+authority+cm',
     "WEB"),
]


# A list of mobile endpoint sites
me_sites = [ ("All", "All"),
    ('urn:publicid:IDN+bus-4208.powderwireless.net+authority+cm',
     "Bus4208"),
    ('urn:publicid:IDN+bus-4329.powderwireless.net+authority+cm',
     "Bus4329"),
    ('urn:publicid:IDN+bus-4330.powderwireless.net+authority+cm',
     "Bus4330"),
    ('urn:publicid:IDN+bus-4407.powderwireless.net+authority+cm',
     "Bus4407"),
    ('urn:publicid:IDN+bus-4408.powderwireless.net+authority+cm',
     "Bus4408"),
    ('urn:publicid:IDN+bus-4409.powderwireless.net+authority+cm',
     "Bus4409"),
    ('urn:publicid:IDN+bus-4410.powderwireless.net+authority+cm',
     "Bus4410"),
    ('urn:publicid:IDN+bus-4555.powderwireless.net+authority+cm',
     "Bus4555"),
    ('urn:publicid:IDN+bus-4603.powderwireless.net+authority+cm',
     "Bus4603"),
    ('urn:publicid:IDN+bus-4604.powderwireless.net+authority+cm',
     "Bus4604"),
    ('urn:publicid:IDN+bus-4734.powderwireless.net+authority+cm',
     "Bus4734"),
    ('urn:publicid:IDN+bus-4817.powderwireless.net+authority+cm',
     "Bus4817"),
    ('urn:publicid:IDN+bus-4964.powderwireless.net+authority+cm',
     "Bus4964"),
    ('urn:publicid:IDN+bus-5175.powderwireless.net+authority+cm',
     "Bus5175"),
    ('urn:publicid:IDN+bus-6180.powderwireless.net+authority+cm',
     "Bus6180"),
    ('urn:publicid:IDN+bus-6181.powderwireless.net+authority+cm',
     "Bus6181"),
    ('urn:publicid:IDN+bus-6182.powderwireless.net+authority+cm',
     "Bus6182"),
    ('urn:publicid:IDN+bus-6183.powderwireless.net+authority+cm',
     "Bus6183"),
    ('urn:publicid:IDN+bus-6185.powderwireless.net+authority+cm',
     "Bus6185"),
    ('urn:publicid:IDN+bus-6186.powderwireless.net+authority+cm',
     "Bus6186"),
]

datasets = {
    "SpectSet": "urn:publicid:IDN+emulab.net:nrdz+ltdataset+SpectSet"
}


# Top-level request object.
request = portal.context.makeRequestRSpec()


# Helper function that allocates a PC + X310 radio pair, with Ethernet
# link between them.
def x310_node_pair(x310_radio_name, node_type, comp_image):
    radio_link = request.Link("%s-link" % x310_radio_name)

    node = request.RawPC("%s-comp" % x310_radio_name)
    node.hardware_type = node_type
    node.disk_image = comp_image

    #node.addService(rspec.Execute(shell="bash",command=x310_setup_cmd))

    node_radio_if = node.addInterface("usrp_if")
    node_radio_if.addAddress(rspec.IPv4Address("192.168.40.1",
                                               "255.255.255.0"))
    radio_link.addInterface(node_radio_if)

    radio = request.RawPC("%s-x310" % x310_radio_name)
    radio.component_id = x310_radio_name
    radio_link.addNode(radio)

# Helper function to connect orch to a dataset
def connect_to_dataset(node, dataset_name):
    # We need a link to talk to the remote file system, so make an interface.
    iface = node.addInterface()

    # The remote file system is represented by special node.
    fsnode = request.RemoteBlockstore("fsnode", "/" + dataset_name)

    # This URN is displayed in the web interfaace for your dataset.
    fsnode.dataset = datasets[dataset_name]
    #
    # The "rwclone" attribute allows you to map a writable copy of the
    # indicated SAN-based dataset. In this way, multiple nodes can map
    # the same dataset simultaneously. In many situations, this is more
    # useful than a "readonly" mapping. For example, a dataset
    # containing a Linux source tree could be mapped into multiple
    # nodes, each of which could do its own independent,
    # non-conflicting configure and build in their respective copies.
    # Currently, rwclones are "ephemeral" in that any changes made are
    # lost when the experiment mapping the clone is terminated.
    #
    fsnode.rwclone = False

    #
    # The "readonly" attribute, like the rwclone attribute, allows you to
    # map a dataset onto multiple nodes simultaneously. But with readonly,
    # those mappings will only allow read access (duh!) and any filesystem
    # (/mydata in this example) will thus be mounted read-only. Currently,
    # readonly mappings are implemented as clones that are exported
    # allowing just read access, so there are minimal efficiency reasons to
    # use a readonly mapping rather than a clone. The main reason to use a
    # readonly mapping is to avoid a situation in which you forget that
    # changes to a clone dataset are ephemeral, and then lose some
    # important changes when you terminate the experiment.
    #
    fsnode.readonly = False
    
    # Now we add the link between the node and the special node
    fslink = request.Link("fslink")
    fslink.addInterface(iface)
    fslink.addInterface(fsnode.interface)

    # Special attributes for this link that we must use.
    fslink.best_effort = True
    fslink.vlan_tagging = True


# Node type parameter for PCs to be paired with X310 radios.
# Restricted to those that are known to work well with them.
portal.context.defineParameter(
    "nodetype",
    "Compute node type",
    portal.ParameterType.STRING, "d740",
    ["d740","d430"],
    "Type of compute node to be paired with the X310 Radios",
)

# Node type for the orchestrator.
portal.context.defineParameter(
    "orchtype",
    "Orchestrator node type",
    portal.ParameterType.STRING, "None",
    ["None", "d430","d740", ""],
    "Type of compute node for the orchestrator (unset == 'any available')",
)

portal.context.defineParameter(
    "dataset",
    "Dataset to connect",
    portal.ParameterType.STRING, "SpectSet",
    ["SpectSet","None"],
    "Name of the remote dataset to connect with orch",
)

portal.context.defineParameter(
    "comp_image",
    "Image for compute node",
    portal.ParameterType.STRING, images_list[0],
    images_list,
    "Image for compute node",
)

portal.context.defineParameter(
    "nuc_image",
    "Image for nuc",
    portal.ParameterType.STRING, images_list[0],
    images_list,
    "Image for nuc",
)




# Set of Monitor X310 radios to allocate
portal.context.defineStructParameter(
    "monitor_radio_sites", "Monitor Radio Sites", [],
    multiValue=True,
    min=0,
    multiValueTitle="Monitor X310 radios.",
    members=[
        portal.Parameter(
            "radio",
            "Monitor Radio Site",
            portal.ParameterType.STRING,
            monitor_radios[0], monitor_radios,
            longDescription="Monitor X310 radio will be allocated from selected site."
        ),
    ])


# Set of Fixed Endpoint devices to allocate (nuc1)
portal.context.defineStructParameter(
    "fe_radio_sites_nuc1", "Fixed Endpoint Sites", [],
    multiValue=True,
    min=0,
    multiValueTitle="Fixed Endpoint NUC1+B210 radios.",
    members=[
        portal.Parameter(
            "site",
            "FE Site",
            portal.ParameterType.STRING,
            fe_sites[0], fe_sites,
            longDescription="A `nuc1` device will be selected at the site."
        ),
    ])


# Set of Mobile Endpoint devices to allocate
portal.context.defineStructParameter(
    "me_radio_sites", "Mobile Endpoint Sites", [],
    multiValue=True,
    min=0,
    multiValueTitle="Mobile Endpoint Supermicro+B210 radios.",
    members=[
        portal.Parameter(
            "site",
            "ME Site",
            portal.ParameterType.STRING,
            me_sites[0], me_sites,
            longDescription="An `ed1` device will be selected at the site."
        ),
    ])


# Bind and verify parameters
params = portal.context.bindParameters()

# Now verify.
portal.context.verifyParameters()

# Get images
nuc_image = images[params.nuc_image]
comp_image = images[params.comp_image]


# Allocate orchestrator node
if params.orchtype != "None":
    orch = request.RawPC("orch")
    orch.disk_image = images["U18-GR"]
    orch.hardware_type = params.orchtype
    if params.dataset != "None": 
        connect_to_dataset(orch, params.dataset)


# Request PC + Monitor X310 resource pairs.
for rsite in params.monitor_radio_sites:
    x310_node_pair(rsite.radio, params.nodetype, comp_image)


# Request nuc1+B210 radio resources at FE sites.
for fesite in params.fe_radio_sites_nuc1:
    nuc = ""
    for urn,sname in fe_sites:
        if urn == fesite.site:
            nuc = request.RawPC("%s-nuc1-b210" % sname)
            break
    nuc.component_manager_id = fesite.site
    nuc.component_id = "nuc1"
    nuc.disk_image = nuc_image


# Request ed1+B210 radio resources at ME sites.
for mesite in params.me_radio_sites:
    if mesite.site == "All":
        obj = request.requestAllRoutes()
        obj.disk_image = nuc_image
    else:
        node = ""
        for urn,sname in me_sites:
            if urn == mesite.site:
                node = request.RawPC("%s-b210" % sname)
                node.component_manager_id = mesite.site
                node.component_id = "ed1"
                node.disk_image = nuc_image
                break
        
    
# Emit!
portal.context.printRequestRSpec()
