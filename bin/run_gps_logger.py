#!/usr/bin/env python3

# General purpose utility functions

import numpy as np
import datetime
import subprocess
import shlex
import multiprocessing as mp
import json
import csv
import os
import time


def start_gps_loggers(buses, out, duration):
        gpsloggers = []
        for bus in buses:
            p = mp.Process(target=run_gps_logger, args=(bus, duration, out, ))
            gpsloggers.append(p)
            p.start()
        return gpsloggers

def run_gps_logger(bus, duration, out):
	glfile = out + "/bus-"+str(bus)+"_locations.csv"
	if os.path.isfile(glfile):
		header = False
	else:
		header = True
	with open(glfile, mode='a') as gpslogfile:
		gpslog = csv.writer(gpslogfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
		if header:
			gpslog.writerow(["Time (UTC)" , "Lat", "Lon", "Alt", "Speed (meters/sec)"])

		command = "timeout %s gpspipe -w control.bus-%s.powderwireless.net" %(duration, bus)
		process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE)
		count = 0 
		while True:
			output = process.stdout.readline()
			if not output and process.poll() is not None:
				break
			try:
				gps_info = json.loads(output)
			except:
				print("Encountered problems in running GPS client for ", bus)
				gpslogfile.flush()
				#gpslogfile.close()
				#exit(1)

			if gps_info['class'] == 'TPV':
				#print(gps_info['time'], datetime.datetime.now(datetime.timezone.utc))
				gpslog.writerow([gps_info['time'], gps_info['lat'], gps_info['lon'], gps_info['alt'], gps_info['speed']])
				#count += 1
				#if count == 60:
				gpslogfile.flush()
				#count = 0
				
		print('Finished bus ', bus)
		gpslogfile.flush()
		gpslogfile.close()
		return process.poll()



gpsduration = 3*60*60
buses = [4817, 6183]
gpsloggers = start_gps_loggers(buses, '/local/', gpsduration)
time.sleep(gpsduration)
for p in gpsloggers:
    p.terminate() 



