#!/bin/bash

#OUT="/proj/NRDZ/falcon_data"
OUT="/local/data/"
FREQ="1980e6"
DURATION="2m"
REPEAT="20"
SLEEP="1s"
WORKERS=("8" "16" "32" "64" "128" "10" "20" "30" "40" "50" "100") 
#WORKERS=("8") 


hostname=$( cat /proc/sys/kernel/hostname )
host=(${hostname//./ })
if [[ $hostname == *"-comp"* ]]; then
  host=(${host//-/ })
  name=${host[1]}
else
  name=${host[0]}
fi
echo $name

mkdir -p $OUT

# Set CPUs into performance mode
gov="performance"
echo "Setting CPU governor to '$gov'"
for file in /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor; do echo "$gov" | sudo tee $file; done


# Allow realtime priority for FalconEye
PROG="/local/tools/falcon/build/src/FalconEye"
CMD="sudo setcap cap_sys_nice=eip $PROG"
$CMD

#exit 0

START=1
for i in $(eval echo "{$START..$REPEAT}")
do
	for worker in ${WORKERS[@]}
	do
		echo "-------------- $i/$REPEAT --------------"
		today=$(date +"%m-%d-%Y")
		now=$(date +"%H-%M-%S")
		out="$OUT"/Falco2_"$name"_"$today"_"$now"_"$worker"
		mkdir "$out"

		/local/tools/falcon/build/src/FalconEye -D $out/dci.csv -E $out/stat.csv -r -N -f $FREQ -W $worker -n 120000 2>&1 | tee $out/log.txt
		#/local/tools/falcon/build/src/FalconEye -D $out/dci.csv -E $out/stat.csv -r -f $FREQ -W $worker -n 120000 2>&1 | tee $out/log.txt

		sleep $SLEEP
	done
done


