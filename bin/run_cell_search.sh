#!/bin/bash

#OUT="/proj/NRDZ/falcon_data"
OUT="/var/emulab/save/"
#OUT="/local/"

B700=("29" "12" "13" "14") # Skipped 17 since it is part of 12
B800=("26") # Skipped 5 since it is part of 26
B2100=("66") # Skipped 4 since it is part of 66
#B1800=("3") # Only band, not used in US
B1900=("25") # Skipped 2 since it is part of 25
B2300=("30") # Only FDD band
#B2600=("7" "69") # Supplementary band, not used in US

#BANDS=("${B700[@]}" "${B800[@]}" "${B2100[@]}" "${B1900[@]}" "${B2300[@]}")
BANDS=("71") 

SLEEP="1s"
DATASET="/SpectSet/LTE/cell_search/"
TRANSFER=""
REPEAT="100"

hostname=$( cat /proc/sys/kernel/hostname )
host=(${hostname//./ })

if [[ $hostname == *"-comp"* ]]; then
  host=(${host//-/ })
  name=${host[1]}
else
  name=${host[0]}
fi
echo $name


START=1
for i in $(eval echo "{$START..$REPEAT}")
do
	for b in ${BANDS[@]}
	do
		echo "-------------- $b ($i/$REPEAT) --------------"

	    out="$OUT"/"$name"_band"$b"
		mkdir -p "$out"
		today=$(date +"%m-%d-%Y")
		now=$(date +"%H-%M-%S")
		
		/local/tools/srsran/build/lib/examples/cell_search -b $b -f "$out"/"$today"_"$now".csv

		sleep $SLEEP

	    if [ ! -z "$TRANSFER" ]
	    then
		    rsync -az --no-o --no-g --no-perms --progress --stats $out_d  aniqua@$TRANSFER:$DATASET/
		fi
	done
done


