#!/bin/bash

#OUT="/proj/NRDZ/falcon_data"
OUT="/local/data/"
FREQ="2120e6"
DURATION="2m"
REPEAT="10"
SLEEP="1s"
WORKERS=("8" "16" "32" "64" "128" "10" "20" "30" "40" "50" "100") 

hostname=$( cat /proc/sys/kernel/hostname )
host=(${hostname//./ })
if [[ $hostname == *"-comp"* ]]; then
  host=(${host//-/ })
  name=${host[1]}
else
  name=${host[0]}
fi
echo $name

mkdir -p $OUT



START=1
for i in $(eval echo "{$START..$REPEAT}")
do
	for worker in ${WORKERS[@]}
	do
		echo "-------------- $i/$REPEAT --------------"
		today=$(date +"%m-%d-%Y")
		now=$(date +"%H-%M-%S")
		out="$OUT"/Falcon_"$name"_"$today"_"$now"_"$worker"
		mkdir "$out"

		timeout -s SIGINT $DURATION /local/tools/falcon/build/src/FalconEye -D $out/dci.csv -E $out/stat.csv -r -N -f $FREQ -W $worker  2>&1 | tee $out/log.txt

		sleep $SLEEP
	done
done


