#!/bin/bash

#OUT="/proj/NRDZ/falcon_data"
OUT="/local/data/"
FREQ="1980e6"
DURATION="2m"
REPEAT="10"
SLEEP="1s"
DATASET="/SpectSet/Falcon_data_raw/"
TRANSFER=""

hostname=$( cat /proc/sys/kernel/hostname )
host=(${hostname//./ })
if [[ $hostname == *"-comp"* ]]; then
  host=(${host//-/ })
  name=${host[1]}
else
  name=${host[0]}
fi
echo $name

mkdir -p $OUT

START=1
for i in $(eval echo "{$START..$REPEAT}")
do
	echo "-------------- $i/$REPEAT --------------"
	today=$(date +"%m-%d-%Y")
	now=$(date +"%H-%M-%S")
	out="$OUT"/Falcon2_"$name"_"$today"_"$now"
	mkdir "$out"

	#timeout -s SIGINT $DURATION /opt/falcon/build/src/FalconEye -D $out/dci.csv -E $out/stat.txt -F $out/log.txt -r -N -f $FREQ
	timeout -s SIGINT $DURATION /local/tools/falcon/build/src/FalconEye -D $out/dci.csv -E $out/stat.csv -r -N -f $FREQ -W 40  2>&1 | tee $out/log.txt

	sleep $SLEEP

    if [ ! -z "$TRANSFER" ]
	then
	    rsync -az --no-o --no-g --no-perms --progress --stats $out  aniqua@$TRANSFER:$DATASET/
	fi
done


