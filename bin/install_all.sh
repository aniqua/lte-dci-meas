#!/bin/bash

sudo apt-get update

sudo apt-get -y install build-essential git subversion cmake libboost-system-dev libboost-test-dev libboost-thread-dev libqwt-dev libqt4-dev libfftw3-dev libsctp-dev libconfig-dev libconfig++-dev libmbedtls-dev

sudo apt-get -y  install libboost-system-dev libboost-test-dev libboost-thread-dev libqwt-qt5-dev qtbase5-dev

cd /opt

sudo git clone --recursive https://github.com/srsLTE/srsGUI.git

cd srsGUI

sudo mkdir build

cd build

sudo cmake ../

sudo make

sudo make install

cd /opt

sudo apt-get -y install libglib2.0-dev libudev-dev libcurl4-gnutls-dev libboost-all-dev qtdeclarative5-dev libqt5charts5-dev

sudo git clone --recursive https://github.com/falkenber9/falcon.git

host=$( cat /proc/sys/kernel/hostname )
if [[ $host == *"-comp"* ]]; then
  sudo cp /local/repository/etc/simd.h /opt/falcon/build/srsLTE-src/lib/include/srslte/phy/utils/simd.h # Need this for X310
fi

sudo sed -i 's/uint32_t max_trial = 3;/uint32_t max_trial = 20;/g' /opt/falcon/src/eye/EyeCore.cc

sudo sed -i 's/uint32_t max_trial = 3;/uint32_t max_trial = 20;/g' /opt/falcon/src/capture_probe/CaptureProbeCore.cc

cd falcon

sudo mkdir build

cd build

sudo cmake ../

sudo make


